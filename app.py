from flask import Flask, request, jsonify, make_response
from sqlalchemy import Integer, String, Date, Float, Column, create_engine, ForeignKey
from sqlalchemy.orm import declarative_base, Session, sessionmaker

# datos conexion Base de datos
user = 'root'
password = ''
host = '127.0.0.1'
port = 3306
database = 'prueba'

# funcion para establecer la conexion
def connection():
    return create_engine(url="mysql+pymysql://{0}:{1}@{2}:{3}/{4}".format(
        user, password, host, port, database
    ), echo=True)

# ORM Models
Base = declarative_base()

# sesion para ejecutar los querys con la conexion
Session = sessionmaker(bind=connection())
session = Session()

# Modelos
# tabla de los cargos
class Cargos(Base):
    __tablename__ = "cargos"

    id = Column("id", Integer, primary_key=True, autoincrement=True)
    cargo = Column("cargo", String(60))
    bonificacion = Column("bonificacion", Integer)

    def __init__(self, cargo, bonificacion):
        self.cargo = cargo
        self.bonificacion = bonificacion

    def json(self):
        return {'id': self.id, 'cargo': self.cargo, 'bonificacion': self.bonificacion}

# tabla de las personas
class Personas(Base):
    __tablename__ = "personas"

    cedula = Column("cedula", Integer, primary_key=True)
    nombres = Column("nombres", String(32))
    apellidos = Column("apellidos", String(32))
    salario = Column("salario", Float)
    cargo_id = Column(Integer, ForeignKey("cargos.id"))
    fechaNacimiento = Column("fechaNacimiento", Date)

    # constructor
    def __init__(self, cedula, nombres, apellidos, salario, cargoId, fechaNacimiento):
        self.cedula = cedula
        self.nombres = nombres
        self.apellidos = apellidos
        self.salario = salario
        self.cargo_id = cargoId
        self.fechaNacimiento = fechaNacimiento
    
    #funcion para retornar los datos
    def json(self, cargo):
        return {'cedula': self.cedula, 'nombres': self.nombres, 'apellidos': self.apellidos, 'salario': self.salario, 'fecha_nacimiento': self.fechaNacimiento, 'cargo': cargo}


app = Flask(__name__)

@app.route('/')
def index():
    return "Hello Mundo"

@app.route('/agregar-cargo', methods=['POST'])
def crear_cargo():
    try:
        data = request.get_json()
        nuevo_cargo = Cargos(cargo=data['cargo'], bonificacion=data['bonificacion'])
        session.add(nuevo_cargo)
        session.commit()
        return make_response(jsonify({'message': 'cargo creado'}), 201)
    except:
        return make_response(jsonify({'message': 'error en el servidor'}), 500)

@app.route('/cargos', methods=['GET'])
def get_cargos():
    cargos = session.query(Cargos).all()
    return make_response(jsonify({'cargos': [cargo.json() for cargo in cargos]}), 200)

@app.route('/agregar-persona', methods=['POST'])
def crear_persona():
    try:
        data = request.get_json()
        nueva_persona = Personas(cedula=data['cedula'], nombres=data['nombres'], apellidos=data['apellidos'],
                                 salario=data['salario'], cargoId=data['cargo_id'], fechaNacimiento=data['fecha_nacimiento'])
        session.add(nueva_persona)
        session.commit()
        return make_response(jsonify({'message': 'persona creada'}), 201)
    except:
        return make_response(jsonify({'message': 'error en el servidor'}), 500)

@app.route('/personas', methods=['GET'])
def get_personas():
    allPersonas = session.query(Personas, Cargos).join(Cargos).all()
    return make_response(jsonify({'personas': [persona.json(cargo.json()) for persona, cargo in allPersonas]}), 200)


if __name__ == '__main__':
    try:
        with connection().connect() as connection:
            
            print(f"Connection to the {host} for user {user} created successfully.")
            Base.metadata.create_all(bind=connection)
    except Exception as ex:
        print("Connection could not be made due to the following error: \n", ex)
    app.run(debug=True)